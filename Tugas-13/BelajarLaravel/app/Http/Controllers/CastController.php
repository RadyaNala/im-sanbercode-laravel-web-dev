<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Untuk menampilkan database
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{   
    //Membuat data baru
    public function create(){
        return view('cast.tambah');
    }
    //Memasukan data ke database
    public function store(Request $request){
        //Validasi data
        $validated = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|min:10'
        ]);
        //Memasukan data ke database
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');
    }
    //Menampilkan data 
    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.tampil', ['cast' => $cast]);
    }
    //Menampilkan data berdasar id
    public function show($id){
        $cast = DB::table('cast')->find($id);
        return view('cast.detail', ['cast' => $cast]);
    }
    //Mengubah data berdasar id
    public function edit($id){
        $cast = DB::table('cast')->find($id);
        return view('cast.edit', ['cast' => $cast]);
    }
    //Mengupdate data ke database
    public function update($id,Request $request){
        $validated = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|min:10'
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' =>$request['umur'],
                'bio' => $request['bio']
                ]);
        //Kembali ke halaman cast
        return redirect('/cast');
    }
    //Delete data berdasar id
        
    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
