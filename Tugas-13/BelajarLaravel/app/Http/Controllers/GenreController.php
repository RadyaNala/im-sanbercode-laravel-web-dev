<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenreController extends Controller
{
    public function create(){
        return view('genre.tambah');
    }
    public function store(Request $request){
        $validated = $request->validate([
            'nama' => 'required'
        ]);
        DB::table('genre')->insert([
            'nama' => $request['nama']
        ]);
        return redirect('/genre');
    }
    public function index(){
        $genre = DB::table('genre')->get();
        return view('genre.tampil', ['genre' => $genre]);
    }

}
