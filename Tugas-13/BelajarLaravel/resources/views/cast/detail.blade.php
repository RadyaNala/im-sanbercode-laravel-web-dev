@extends('layout.master')

@section('title')
Halaman Detail Cast
@endsection

@section('content')
    <h1>{{$cast->nama}}</h1>
    <p>{{$cast->umur}}  tahun<br> {{$cast->bio}}</p>
@endsection