@extends('layout.master')

@section('title')
Halaman Tambah Cast
@endsection

@section('content')
<form action="/cast" method="POST">
  @csrf
  <div class="form-group">
    <label for="namaCast">Nama</label>
    <input type="text" class="form-control" id="namaCast" name='nama'>
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="umurCast">Umur</label>
    <input type="number" class="form-control" id="umurCast" name='umur'>
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="bioCast">Bio</label>
    <textarea name="bio" id="bioCast" cols="30" rows="10" class="form-control"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

