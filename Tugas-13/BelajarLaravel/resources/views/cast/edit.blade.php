@extends('layout.master')

@section('title')
Halaman Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
  <div class="form-group">
    <label for="namaCast">Nama</label>
    <input type="text" class="form-control" id="namaCast" name='nama' value="{{$cast->nama}}">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="umurCast">Umur</label>
    <input type="number" class="form-control" id="umurCast" name='umur' value="{{$cast->umur}}">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="bioCast">Bio</label>
    <textarea name="bio" id="bioCast" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection