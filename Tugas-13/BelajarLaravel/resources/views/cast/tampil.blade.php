@extends('layout.master')

@section('title')
Halaman List Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah Cast</a>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nama</th>
          <th scope="col"><center>Opsi</center></th>
        </tr>
      </thead>
      <tbody>

        @forelse ($cast as $key => $value)
          <tr>
            <td>{{$key +1}}</td>
            <td>{{$value->nama}}</td>
            <td>
              <center>
                <form action="/cast/{{$value->id}}" method='post'>
                  @csrf
                  @method('delete')
                  <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </center>
            </td>
          </tr>
        @empty
            <tr>
              <td>Data Cast Kosong</td>
            </tr>
        @endforelse
      </tbody>
    </table>
@endsection
