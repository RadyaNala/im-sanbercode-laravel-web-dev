@extends('layout.master')

@section('title')
Halaman Tambah Genre
@endsection

@section('content')
<form action="/genre" method="POST">
  @csrf
  <div class="form-group">
    <label for="namaGenre">Nama Genre</label>
    <input type="text" class="form-control" id="namaGenre" name='nama'>
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection