@extends('layout.master')

@section('title')
Halaman List Kategori
@endsection

@section('content')
    <a href="/genre/create" class="btn btn-primary btn-sm mb-3">Tambah Genre</a>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nama</th>
          <th scope="col">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($genre as $key => $value)
          <tr>
            <td>{{$key +1}}</td>
            <td>{{$value->nama}}</td>
            <td><a href="" class="btn btn-info btn-sm">Detail</a></td>
          </tr>
        @empty
            <tr>
              <td>Tidak Ada</td>
            </tr>
        @endforelse
      </tbody>
    </table>
@endsection