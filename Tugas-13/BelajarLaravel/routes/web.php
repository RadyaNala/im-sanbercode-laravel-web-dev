<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'dashboard']);
Route::get('/register',[AuthController::class, 'register'] );
Route::get('/table',[AuthController::class, 'table'] );
Route::get('/data-tables',[AuthController::class, 'datatables'] );
Route::post('/welcome', [AuthController::class,'signUp']);


//CRUD GENRE

//Create
//Form tambah genre
Route::get('/genre/create',[GenreController::class, 'create']);

//Tambah data ke database
Route::post('/genre',[GenreController::class, 'store']);

//Read
//Menampilkan semua data
Route::get('/genre',[GenreController::class, 'index']);

//CRUD CAST
//CREATE DATA
//ROUTE KE FORM TAMBAH CAST
Route::get('/cast/create',[CastController::class, 'create']);

//ROUTE SIMPAN DATA KE DATABASE
Route::post('/cast',[CastController::class, 'store']);

//READ
//MENAMPILKAN SEMUA DATA
Route::get('/cast',[CastController::class, 'index']);
//MENAMPILKAN DATA BERDASARKAN ID
Route::get('/cast/{id}',[CastController::class, 'show']);

//UPDATE
//MENGUBAH DATA DATABASE
Route::get('/cast/{id}/edit',[CastController::class, 'edit']);
//UPDATE CAST BERDASAR ID
Route::put('/cast/{id}',[CastController::class, 'update']);

//DELETE
//MENGHAPUS DATA BERDASAR ID
Route::delete('/cast/{id}',[CastController::class, 'destroy']);