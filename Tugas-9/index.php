<?php
  require('animal.php');
  require('ape.php');
  require('frog.php');
  
  $sheep = new Animal("shaun");
  echo "Name : " . $sheep->name . "<br>";
  echo "Legs : " . $sheep->legs. "<br>";
  echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>";

  $kodok = new frog("buduk");
  echo "Name : " . $kodok->name . "<br>Legs : ";
  echo "" . $kodok->legs. "<br>";
  echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
  echo $kodok->jump()."<br><br>";

  $sungokong = new Ape("Kera Sakti");
  echo "Name : " . $sungokong->name . "<br>";
  echo "Legs : " . $sungokong->legs. "<br>";
  echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
  echo $sungokong->yell() ."<br>";
?>