<!DOCTYPE html>
<html lang="en">
<head>
    <title>Form</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First name:</label> 
            <br><br>
            <input type="text" name="namaDepan" id='firstname'> 
            <br><br>
            <label for="lastname">Last name:</label>  
            <br><br>
            <input type="text" name="namaBelakang" id='lastname'>
            <br><br>
        <label> Gender:</label> 
            <br><br>
            <input type="radio" name="gender" id='m'>
            <label for="m">Male</label><br>
            <input type="radio" name="gender" id='f'>
            <label for="f">Female</label><br>
            <input type="radio" name="gender" id='o'>
            <label for="o">Other</label><br>
            <br>
        <label>Nationality:</label>
            <br><br>
        <select name="nationality">
            <option value="">Indonesian</option>
            <option value="">Singapore</option>
            <option value="">English</option>
            <option value="">Australian</option>
        </select>
            <br><br>
        <label>Language Spoken:</label>
            <br><br>
            <input type="checkbox" name="languagespoken" id='indo'><label for="indo">Indonesian</label> <br>
            <input type="checkbox" name="languagespoken" id='en'><label for="en">English</label> <br>
            <input type="checkbox" name="languagespoken" id='ot'><label for="ot">Other</label> <br>
        </select>
            <br><br>
        <label for='bio'>Bio:</label>
            <br><br>
        <textarea cols="35" rows="15" id='bio'></textarea>
            <br>
        <input type="submit" value="Kirim">
    </form>
</body>
</html>

