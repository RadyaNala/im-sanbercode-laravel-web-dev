<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{   
    public function register(){
        return view('page.register');
    }
    public function signUp(Request $request){
        $namaDepan = $request['namaDepan'];
        $namaBelakang = $request['namaBelakang'];
        return view('welcome', ['namaDepan' => $namaDepan , 'namaBelakang' => $namaBelakang,]);
    }
}
